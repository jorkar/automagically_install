#!/bin/bash
#install script for automagically
# tar cvfz am, am2
#
ln -s /home/pi/source/automagically/configuration/automagically.init.d /etc/init.d/automagically
ln -s /home/pi/source/automagically/configuration/ports.conf /etc/apache2/ports.conf 
ln -s /home/pi/source/automagically/configuration/httpd.conf /etc/apache2/sites-enabled/automagically 
ln -s /home/pi/source/automagically/configuration/automagically.init.d /etc/init.d/automagically
ln -s /home/pi/source/automagically/configuration/my2.cnf /etc/mysql/my.cnf
ln -s /home/pi/source/automagically/bin/libcec.so.1 /usr/local/lib/libcec.so
ln -s /home/pi/source/automagically/bin/libcec.so.1 /usr/local/lib/libcec.so.1
#
# make automagically run on boot
#
update-rc.d automagically defaults
#
/etc/init.d/apache2 restart
/etc/init.d/mysql restart
/etc/init.d/automagically restart
