# Installation of automagically

This is not a fork of [automagically][1], it is merely installation instructions if you do not want
to install the entire package as described [here][1].

After installation using these instructions it is possible to update to the latest release using
the built in update functionality.

[Automagically][1] is free for non commercial use, no commercial disitribution allowed. 
So this applies to this package aswell. 

## Latest version

Install package: 

	automagically_20130505
	Based on sourcecode revision: fd8318b
	Source repository and issue tracker: [https://bitbucket.org/davka003/automagically/][2]
	Source repository and issue tracker for installer: [https://bitbucket.org/jorkar/automagically_install/][3]


## Prerequsisites

Before starting installing automagically you should have a debian based installation. I have tested on a raspberry pi running wheezy. 
However it should work on any debian based platform.

A good starting point for a Rapberry Pi is to download Raspibian: [http://downloads.raspberrypi.org/raspbian_latest][4]

Instructions how to write the SD card can be found here: [http://elinux.org/RPi_Easy_SD_Card_Setup#Flashing_the_SD_Card_using_Windows][5]

When booting Raspian the first time, it will ask to configure the system so it is reccomended to have it connected to a monitor and a keyboard.
In the configuration (raspi-config), set your timezone and expand the root file system to make most use of your SD card.

After first boot is completed, you might want to update the system. These are the commands to execute for that.

	$ sudo apt-get update
	$ sudo apt-get upgrade
	$ sudo apt-get dist-upgrade
	$ sudo apt-get clean
	$ sudo apt-get autoclean 

On-top of Raspbian a number of applications needs to be installed. Each of them will be described below:

	* apache2
	* mysql
	* python 
	* git
	* tellstick-core

#Installation

## Apache2

To install Apache2, perform the following commands

	$ sudo apt-get install apache2 libapache2-mod-wsgi

For now, disable all sites you have in apache2:

	$ sudo a2dissite default
	$ sudo service apache2 reload

Then stop apache

	$ sudo /etc/init.d/apache2 stop

## MySQL

To install MySQL, perform the following commands:

	$ sudo apt-get install mysql-server mysql-client

When asked, give root the password 'raspberry'

## Git

To install Git, perform the following commands:

	$ sudo apt-get install git 

## Install Python tools and django

	$ sudo apt-get install python python-mysqldb python-django python-pip python-matplotlib python-setuptools
	$ sudo pip install south
	$ sudo easy_install --upgrade pytz
	$ sudo pip install pyephem

## Install telldus-core
To install telldus-core, follow the instruction in Embedded Linux Wiki: [http://elinux.org/R-Pi_Tellstick_core][6]

## Install automagically

### Install automagically

	$ cd ~
	$ wget https://bitbucket.org/jorkar/automagically_install/downloads/automagically_20130505.tar.gz
	$ tar xvfz automagically_20130505.tar.gz

Note change to latest version

	$ mysql -uroot –p
	  ==> Enter password: raspberry
	  ==> mysql> create database automagically;
	  ==> mysql> exit

	$ mysql -uroot -praspberry automagically < dist/am.sql
	$ tar xvfz dist/am.tar.gz 
	$ tar xvfz dist/am2.tar.gz

#### Run install script

	$ sudo ./install.sh
	
#### Fix som rights

	$ sudo chmod a+w /etc/tellstick.conf
	$ chmod 777 /home/pi/source/automagically/daemon/daemon_input
	$ chmod 777 /home/pi/source/automagically/daemon/daemon_output
	
### Update to latest automagically

	$ ~/source/automagically/update.sh

### Cleanup

Cleanup some files no longer needed

	$ rm -rf ~/install.sh
	$ rm -rf ~/dist
	$ rm -rf ~/automagically_20130505.tar.gz
	$ sudo rm -rf /usr/src/telldus-core-2.1.1

### Bootstrap automagically

Make automagically start at boot

	$ sudo update-rc.d automagically defaults

## Start automagically

	$ sudo /etc/init.d/apache2 start
	$ sudo /etc/init.d/automagically start

## Cleanse Automagically
	http://<your rpi>/core/  (wait 60 sec)
	http://<your rpi>/admin/ (wait 60 sec)
	http://<your rpi>/admin/core/
	  - Remove all /core/ items
	http://<your rpi>/admin/signals/
	  - Remove all /signals/ items

# Finalize your installation

Surf to http://<your rpi>/ and then goto [this site][1] for more information on usage and how to update to the latest version. 

## Configure automagically
	http://<your rpi>/admin/settings/settingsvalue/
	  - Set Longitude and Latitude
	  - Disable signaldebug
	  - Disable hdmi-cec

## Read your tellstick.conf (optional)
	http://<your rpi>/read_config/

[1]: http://automagically.weebly.com/home-automation.html
[2]: https://bitbucket.org/davka003/automagically/
[3]: https://bitbucket.org/jorkar/automagically_install/
[4]: http://downloads.raspberrypi.org/raspbian_latest
[5]: http://elinux.org/RPi_Easy_SD_Card_Setup#Flashing_the_SD_Card_using_Windows
[6]: http://elinux.org/R-Pi_Tellstick_core